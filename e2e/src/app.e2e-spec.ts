import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should be able to add numbers', () => {
    page.navigateTo();
    page.setFirst(1);
    page.setSecond(2);

    expect(page.getResult()).toBe('3');
  });
});
