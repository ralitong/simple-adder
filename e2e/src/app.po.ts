import { browser, by, element, $ } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getResult(): any {
    return $('#result').getText();
  }
  setSecond(num: number): any {
   $('#first-input').sendKeys(num);
  }
  setFirst(num: number): any {
    $('#second-input').sendKeys(num);
  }

}
