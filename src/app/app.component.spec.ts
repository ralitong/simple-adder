import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
  }));


  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should be able to add two numbers', () => {
    app.first = 2;
    app.second = 2;
    fixture.detectChanges();
    const element = getElement('#result');
    expect(element.nativeElement.textContent).toBe('4');
  });


  function getElement(cssSelector: string) {
    return fixture.debugElement.query(By.css(cssSelector));
  }

});


