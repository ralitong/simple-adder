import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'simple-adder';
  first: number = null;
  second: number = null;

  addResult() {
    if (this.first || this.second) {
      return Number(this.first) + Number(this.second);
    }
    return null;
  }
}
